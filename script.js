const button = document.querySelector('.button')
button.addEventListener('click', getInfo)

async function getInfo(){
    try {
        const response = await fetch('https://api.ipify.org/?format=json')
	    const getIp = await response.json()
        
        const responce = await fetch(`http://ip-api.com/json/${getIp.ip}`)
        const data = await responce.json()

        const {continent, country, regionName, city, district} = data

        const values = Object.values({continent, country, regionName, city, district})
        const keys = ['Continent', 'Country', 'Region', 'City', 'District']
        const div = document.querySelector('div') 
        const list = document.createElement('ul')

        values.forEach((value, i)=> {
           
            if (value === undefined) {
                list.innerHTML += `<li>${keys[i]}: Information not found</li>`
            } else {
                list.innerHTML +=  `<li>${keys[i]}: ${value}</li>`
            }

            div.append(list)
        });
    button.disabled = true
    } catch (error) {
    }
}



